!process.env.NODE_ENV ? (process.env.NODE_ENV = 'development') : null

const express = require('express')
const movieroutes = require('./src/routes/movie.routes')
const studioroutes = require('./src/routes/studio.routes')
const authroutes = require('./src/routes/authentication.routes')
const logger = require('./src/config/config').logger

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())

// Add CORS headers
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  )
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type,authorization'
  )
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

app.all('*', (req, res, next) => {
  const method = req.method
  logger.debug('Method: ', method)
  next()
})

app.use('/api', authroutes)
app.use('/api/movies', movieroutes)
app.use('/api', studioroutes)

// Catch-all route
app.all('*', (req, res, next) => {
  logger.warn('Catch-all endpoint aangeroepen')
  next({
    message: req.method + " endpoint '" + req.url + "' does not exist",
    errCode: 401
  })
})

// Errorhandler
app.use((err, req, res, next) => {
  logger.error(err.message)
  res.status(err.errCode || 500).json({
    error: err.error || 'Some error occurred',
    message: err.message
  })
})

app.listen(port, () => {
  logger.info(`Server listening at port ${port}`)
  logger.info(`Server running in '${process.env.NODE_ENV}' mode`)
})

module.exports = app
