const express = require('express')
const router = express.Router()
const moviecontroller = require('../controllers/movie.controller')
const authcontroller = require('../controllers/authentication.controller')

// Movie routes
router.post(
  '',
  authcontroller.validateToken,
  moviecontroller.validateMovie,
  moviecontroller.createMovie
)
router.get('', moviecontroller.getAll)
router.get('/:movieId', moviecontroller.getById)
router.put(
  '/:movieId',
  authcontroller.validateToken,
  moviecontroller.validateMovie,
  moviecontroller.update
)
router.delete(
  '/:movieId',
  authcontroller.validateToken,
  moviecontroller.deleteById
)

module.exports = router
