const express = require('express')
const router = express.Router()
const studioController = require('../controllers/studio.controller')

router.get('/studio', studioController.getAll)

module.exports = router
