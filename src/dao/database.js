const mysql = require('mysql')
const logger = require('../config/config').logger
const dbconfig = require('../config/config').dbconfig

const pool = mysql.createPool(dbconfig)

pool.on('connection', function (connection) {
  logger.trace('Database connection established')
})

pool.on('acquire', function (connection) {
  logger.trace('Database connection aquired')
})

pool.on('release', function (connection) {
  logger.trace('Database connection released')
})

let query = (sqlQuery, sqlValues, callback) => {
  pool.getConnection(function (err, connection) {
    if (err) {
      logger.error(err.message)
      callback(err.message, undefined)
    }
    if (connection) {
      connection.query(sqlQuery, sqlValues, (error, results, fields) => {
        connection.release()
        if (error) {
          logger.error('query', error.toString())
          callback(error.message, undefined)
        }
        if (results) {
          callback(undefined, results)
        }
      })
    }
  })
}

module.exports = { query, pool }
