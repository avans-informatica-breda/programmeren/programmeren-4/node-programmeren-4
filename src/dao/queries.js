module.exports = {
  MOVIE_INSERT:
    'INSERT INTO `movie` (`name`, `releaseyear`, `studioid`, `ageCategory`, `inTheatres`, `userid`)' +
    ' VALUES (?, ?, ?, ?, ?, ?)',

  MOVIE_SELECT: `
SELECT
movie.id,
movie.name,
movie.releaseyear,
studio.studioname as studio,
movie.ageCategory,
movie.inTheatres,
user.ID as userid,
CONCAT(user.firstname, ' ', user.lastname) as createdBy
FROM movie
LEFT JOIN user ON movie.userid = user.id
LEFT JOIN studio ON movie.studioid = studio.id
`,

  MOVIE_ACTORS: `
SELECT
actor.id,
actor.actorname,
movieactor.rolename,
CONCAT(user.firstname, ' ', user.lastname) as createdBy
FROM movieactor
LEFT JOIN user ON userid = user.id
LEFT JOIN actor ON actorid = actor.id
WHERE movieId = ?
;
	`
}
