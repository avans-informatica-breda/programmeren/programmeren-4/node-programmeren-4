const config = require('../config/config')
const queries = require('./queries')
const db = require('./database')
const logger = config.logger

const movieDao = {
  create: (movie, userId, callback) => {
    logger.debug('create', movie)

    let { name, releaseyear, ageCategory, inTheatres, studioid } = movie

    db.query(
      queries.MOVIE_INSERT,
      [name, releaseyear, studioid, ageCategory, inTheatres, userId],
      (err, results) => {
        if (err) {
          logger.trace('createMovie', err)
          callback(err, undefined)
        }
        if (results) {
          logger.trace('results: ', results)
          const insertedMovie = {
            id: results.insertId,
            ...movie
          }
          callback(undefined, insertedMovie)
        }
      }
    )
  },

  list: (query, callback) => {
    logger.debug('list')

    db.query(query, [], (err, results) => {
      if (err) {
        logger.trace('list', err)
        callback(err, undefined)
      }
      if (results) {
        callback(undefined, results)
      }
    })
  },

  getById: (id, callback) => {
    logger.debug('getById', id)

    const query =
      queries.MOVIE_SELECT + ' WHERE movie.id = ?;' + queries.MOVIE_ACTORS

    db.query(query, [id, id], (err, results) => {
      if (err) {
        logger.trace('getById', err)
        callback(err, undefined)
      }
      if (results) {
        const movieInfo = {
          movie: results[0],
          actors: results[1]
        }
        callback(undefined, movieInfo)
      }
    })
  }
}

module.exports = movieDao
