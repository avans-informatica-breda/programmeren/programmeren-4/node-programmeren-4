process.env.DB_DATABASE = process.env.DB_DATABASE || 'movies_testdb'
process.env.NODE_ENV = 'testing'
process.env.LOGLEVEL = 'trace'

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../index')
const pool = require('../../src/dao/database').pool
const logger = require('../../src/config/config').logger
const jwt = require('jsonwebtoken')
const assert = require('assert')

chai.should()
chai.use(chaiHttp)

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`)

/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * Let ook op dat je in de dbconfig de optie multipleStatements: true toevoegt!
 */
/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * LET OOK OP dat je in de dbconfig de optie 'multipleStatements: true' toevoegt!
 */
const CLEAR_MOVIEACTORS_TABLE = 'DELETE IGNORE FROM `movieactor`;'
const CLEAR_ACTORS_TABLE = 'DELETE IGNORE FROM `actor`;'
const CLEAR_MOVIE_TABLE = 'DELETE IGNORE FROM `movie`;'
const CLEAR_STUDIO_TABLE = 'DELETE IGNORE FROM `studio`;'
const CLEAR_USERS_TABLE = 'DELETE IGNORE FROM `user`;'
const CLEAR_DB =
  CLEAR_MOVIEACTORS_TABLE +
  CLEAR_ACTORS_TABLE +
  CLEAR_MOVIE_TABLE +
  CLEAR_STUDIO_TABLE +
  CLEAR_USERS_TABLE

/**
 * Voeg een user toe aan de database. Deze user heeft id 1.
 * Deze id kun je als foreign key gebruiken in de andere queries, bv insert studenthomes.
 */
const INSERT_USER =
  'INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `studentnr`, `password` ) VALUES' +
  '(1, "first", "last", "name@server.nl","1234567", "secret");'

/**
 * Query om twee movies toe te voegen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */
const INSERT_MOVIES =
  'INSERT INTO `movie` (`id`, `name`, `releaseyear`, `studioid`, `userid`) VALUES ' +
  "(1, 'Movie A', 2010, 1, 1)," +
  "(2, 'Movie B', 2020, 2, 1);"

const INSERT_STUDIO =
  "INSERT INTO `studio` (`id`, `studioname`, `userid`) VALUES (1, 'Paramount', 1), (2, 'Pixar', 1);"

/**
 * Deze before staat buiten alle describes() en wordt daarom 1 x aangeroepen
 * voorafgaand aan de buitenste describe.
 */
before((done) => {
  pool.query(CLEAR_DB, (err, rows, fields) => {
    if (err) {
      done(err)
    }
    if (rows) {
      pool.query(INSERT_USER + INSERT_STUDIO, (err, rows, fields) => {
        if (err) {
          done(err)
        } else {
          done()
        }
      })
    }
  })
})

/**
 * Deze after staat buiten alle describes() en wordt daarom 1 x aangeroepen
 * na uitvoering van de buitenste describe. Merk op dat deze after niet per sé
 * nodig is, omdat de before altijd start met een lege database.
 */
after((done) => {
  pool.query(CLEAR_DB, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      logger.info('After FINISHED')
      done()
    }
  })
})

describe('Manage movies', () => {
  describe('UC201 Create movie - POST /api/movies', () => {
    beforeEach((done) => {
      pool.query(CLEAR_MOVIE_TABLE, (err, rows, fields) => {
        if (err) {
          logger.error(`beforeEach CLEAR_MOVIE_TABLE: ${err}`)
          done(err)
        }
        if (rows) {
          done()
        }
      })
    })

    it('TC-201-1 should return valid error when required value is not present', (done) => {
      chai
        .request(server)
        .post('/api/movies')
        // Hier gebruiken we de synchrone jwt.sign om een token te krijgen
        // zodat we simuleren dat we als user met id=1 ingelogd zijn
        .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
        .send({
          // name is missing
          releaseyear: 1234,
          studioid: 1,
          ageCategory: 'children',
          inTheatres: 1
        })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.an('string').that.equals('Error!')
          error.should.be.an('string')

          done()
        })
    })

    it('TC-201-2 should return a valid error when postal code is invalid', (done) => {
      done()
    })

    it('TC-201-6 should successfully add an item when posting valid values', (done) => {
      // Dit is de asynchrone variant van jwt.sign om een token te krijgen.
      jwt.sign({ id: 1 }, 'secret', { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .post('/api/movies')
          .set('authorization', 'Bearer ' + token)
          .send({
            name: 'a name',
            releaseyear: 1234,
            studioid: 1,
            ageCategory: 'children',
            inTheatres: 1
          })
          .end((err, res) => {
            assert.ifError(err)
            res.should.have.status(200)
            res.body.should.be.an('object').that.has.property('result')

            let result = res.body.result
            result.should.be
              .an('object')
              .that.has.all.keys(
                'id',
                'name',
                'releaseyear',
                'studioid',
                'ageCategory',
                'inTheatres'
              )
            result.should.be
              .an('object')
              .that.has.property('id')
              .that.is.a('number')
            done()
          })
      })
    })
  })

  describe('UC202 List movies - GET /api/movies', () => {
    beforeEach((done) => {
      pool.query(CLEAR_MOVIE_TABLE, (err, rows, fields) => {
        if (err) {
          logger.error(`before List Movies: ${err}`)
          done(err)
        } else {
          done()
        }
      })
    })

    it('TC-202-1 should return empty list when database contains no items', (done) => {
      chai
        .request(server)
        .get('/api/movies')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('array').that.has.length(0)

          done()
        })
    })

    it('TC-202-2 should show 2 results', (done) => {
      pool.query(INSERT_MOVIES, (error, result) => {
        if (error) {
          done(error)
        }
        if (result) {
          chai
            .request(server)
            .get('/api/movies')
            .end((err, res) => {
              assert.ifError(err)
              res.should.have.status(200)
              res.should.be.an('object')

              let { result } = res.body
              result.should.be.an('array').that.has.length(2)

              let { id, name, releaseyear, studio } = result[0]
              name.should.be.a('string').that.equals('Movie A')
              releaseyear.should.be.a('number').that.equals(2010)
              studio.should.be.a('string').that.equals('Paramount')
              id.should.be.a('number').that.is.at.least(0)

              done()
            })
        }
      })
    })

    it('TC-202-3 should handle `?studioname=` query param', (done) => {
      pool.query(INSERT_MOVIES, (error, result) => {
        if (error) {
          done(error)
        }
        if (result) {
          chai
            .request(server)
            .get('/api/movies?studioname=Paramount')
            .end((err, res) => {
              assert.ifError(err)
              res.should.have.status(200)
              res.should.be.an('object')

              let { result } = res.body
              result.should.be.an('array').that.has.length(1)

              let { id, name, releaseyear, studio } = result[0]
              name.should.be.a('string').that.equals('Movie A')
              releaseyear.should.be.a('number').that.equals(2010)
              studio.should.be.a('string').that.equals('Paramount')
              id.should.be.a('number').that.is.at.least(0)

              done()
            })
        }
      })
    })

    it('TC-202-4 should handle `?name=` query param', (done) => {
      pool.query(INSERT_MOVIES, (error, result) => {
        if (error) {
          done(error)
        }
        if (result) {
          chai
            .request(server)
            .get('/api/movies?name=Movie B')
            .end((err, res) => {
              assert.ifError(err)
              res.should.have.status(200)
              res.should.be.an('object')

              let { result } = res.body
              result.should.be.an('array').that.has.length(1)

              let { id, name, releaseyear, studio } = result[0]
              name.should.be.a('string').that.equals('Movie B')
              releaseyear.should.be.a('number').that.equals(2020)
              studio.should.be.a('string').that.equals('Pixar')
              id.should.be.a('number').that.is.at.least(0)

              done()
            })
        }
      })
    })
  })
})
