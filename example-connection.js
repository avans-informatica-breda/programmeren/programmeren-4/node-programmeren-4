const mysql = require('mysql')

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'movies_user',
  password: 'secret',
  database: 'movies'
})

connection.connect(function (err) {
  if (err) throw err // not connected!
  console.log('connected as id ' + connection.threadId)
})

// Use the connection
connection.query('SELECT * FROM movies', function (error, results, fields) {
  if (error) throw error

  console.log('results: ', results)
})

// Use the connection
connection.query('SELECT * FROM movies', function (error, results, fields) {
  if (error) throw error

  console.log('results: ', results)
})

function gracefulShutdown() {
  connection.end((err) => {
    console.log('connection ended')
  })
}

// e.g. kill
process.on('SIGTERM', gracefulShutdown)
// e.g. Ctrl + C
process.on('SIGINT', gracefulShutdown)
