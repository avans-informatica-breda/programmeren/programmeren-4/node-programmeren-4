-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `movieactor` ;
DROP TABLE IF EXISTS `actor` ;
DROP TABLE IF EXISTS `movie` ;
DROP TABLE IF EXISTS `studio` ;
DROP TABLE IF EXISTS `user` ;

CREATE TABLE IF NOT EXISTS `user` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(32) NOT NULL,
	`lastname` VARCHAR(32) NOT NULL,
	`email` VARCHAR(32) NOT NULL UNIQUE,
	`studentnr` VARCHAR(32) NOT NULL,
	`password` CHAR(64) BINARY NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

-- Voorbeeld insert query. Wanneer je in Nodejs de ? variant gebruikt hoeven de '' niet om de waarden.
-- Zet die dan wel in het array er na, in de goede volgorde.
-- In je Nodejs app zou het password wel encrypted moeten worden.
INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `studentnr`, `password` ) VALUES
(1, 'Jan', 'Smit', 'j.smit@server.nl','123456', 'secret'),
(2, 'Marieke', 'Jansen', 'm.jansen@mail.nl','222222', 'secret');

-- -----------------------------------------------------
-- Table `studio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `studio` ;
CREATE TABLE IF NOT EXISTS `studio` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`studioname` VARCHAR(128) NOT NULL UNIQUE,
	`userid` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `studio`
ADD CONSTRAINT `fk_studio_user`
FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE;

INSERT INTO `studio` (`id`, `studioname`, `userid`) VALUES
(1, 'Paramount', 1),
(2, 'Warner Brothers', 2),
(3, 'Disney', 1),
(4, 'Metro Goldwyn Mayer', 2),
(5, 'Pixar Studios', 1);

-- -----------------------------------------------------
-- Table `actor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `actor` ;
CREATE TABLE IF NOT EXISTS `actor` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`actorname` VARCHAR(128) NOT NULL,
	`bio` VARCHAR(128),
	`userid` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `actor`
ADD CONSTRAINT `fk_actor_user`
FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE;

INSERT INTO `actor` (`id`, `actorname`, `userid`) VALUES
(1, 'Nemo', 1),
(2, 'Dory', 2),
(3, 'Bruce', 1),
(4, 'Sterre', 2),
(5, 'Darla', 1);

-- -----------------------------------------------------
-- Koppeltabel Movie/Actors
-- -----------------------------------------------------
DROP TABLE IF EXISTS `movieactor` ;
CREATE TABLE IF NOT EXISTS `movieactor` (
	`movieId` INT UNSIGNED NOT NULL,
	`actorId` INT UNSIGNED NOT NULL,
	`rolename` VARCHAR(128) NOT NULL,
	`userId` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`movieId`, `actorId`)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `movie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `movie` ;
CREATE TABLE IF NOT EXISTS `movie` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL UNIQUE,
	`releaseyear` INT UNSIGNED,
	`studioid` INT UNSIGNED NOT NULL,
	`ageCategory` ENUM('all', 'children', 'adults') DEFAULT 'all',
	`inTheatres` BOOL DEFAULT 0, -- 0 = false, 1 = true
	`userid` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `movie`
ADD CONSTRAINT `fk_movie_user`
FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE,
ADD CONSTRAINT `fk_movie_studio`
FOREIGN KEY (`studioid`) REFERENCES `studio` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE;

-- Voorbeeld insert query. Wanneer je in Nodejs de ? variant gebruikt hoeven de '' niet om de waarden.
INSERT INTO `movie` (`name`, `releaseyear`, `studioid`, `ageCategory`, `inTheatres`, `userid`) VALUES
('Finding Nemo', 2003, 5, 'all', 1, 1),
('Inception', 2010, 2, 'children', 1, 2),
('Blade Runner', 1982, 1, 'children', 1, 2),
('Goodfellas', 1990, 1, 'adults', 1, 1);

ALTER TABLE `movieactor`
ADD CONSTRAINT `fk_movieactor_user`
FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE,

ADD CONSTRAINT `fk_movieactor_movie`
FOREIGN KEY (`movieId`) REFERENCES `movie` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE,

ADD CONSTRAINT `fk_movieactor_actor`
FOREIGN KEY (`actorId`) REFERENCES `actor` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE;

INSERT INTO `movieactor` (`movieId`, `actorId`, `rolename`, `userId`) VALUES
(1, 1, 'The star', 1),
(1, 2, 'The blue fish', 1),
(1, 3, 'The big bad shark', 1),
(1, 4, 'The seastar', 1),
(1, 5, 'The dentists niece', 1);
