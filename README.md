# Avans Programmeren 4 example server

Dit is voorbeeldcode bij de lessen Programmeren 4. Deze server gaat uit van een Movie-casus die niet beschreven staat in een functioneel ontwerp, maar aansluit bij bv de Samen Eten casus uit Programmeren 4.

## Installing and starting

Om te installeren run je:

```
npm install
npm run dev
```

## Testing

Om te testen run je:

```
npm test
```

## Codecoverage en Sonar analysis

Run:

```
npm run coverage
npm run sonar
```

## Design

### Database design

![Movies DB design](./doc/movies.png 'Movies database design')
